//
// Created by Landon on 2020-01-20.
//

#ifndef COMP_166_LAB6_ARRAYSEARCH_H
#define COMP_166_LAB6_ARRAYSEARCH_H

int linearSearch (int value, const int numbers[], int nNumbers);

#endif //COMP_166_LAB6_ARRAYSEARCH_H
