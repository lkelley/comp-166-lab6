#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

#include "arraySearch.h"

static int binaryNumbersFile; // File handle for the numbers file
static int *numbers; // Reference for the eventual numbers array


int main(int cnt, char *argv[]) {
    /*
     * Check argument count
     */
    if (cnt != 3) {
        puts("Usage: search numberFileName nNumbers\n");
        return EXIT_FAILURE;
    }

    /*
     * Try to open the numbers file
     */
    if ((binaryNumbersFile = open(argv[1], O_RDONLY)) == -1) {
        printf("Unable to open file \"%s\"", argv[1]);
        return EXIT_FAILURE;
    }

    // Get number of numbers
    long fileNumberCount = strtol(argv[2], NULL, 10);

    // Allocate numbers array
    numbers = (int *)malloc(sizeof(int) * fileNumberCount);

    // Read data into array
    read(binaryNumbersFile, numbers, sizeof(int) * fileNumberCount);

    // Close numbers file
    close(binaryNumbersFile);

    while (1) {
        /*
         * Prompt for search value
         */
        char input[256];
        puts("Enter the integer value to find ('q' to quit):\t");
        scanf("%255s", input);

        // Break loop on 'q' entered
        if (strcmp(input, "q") == 0) break;

        /*
         * Skip on empty entry
         *
         * Yes, this could be input[0] == '\0', but strcmp is more obvious I feel
         */
        if (strcmp(input, "") == 0) {
            puts("Please enter a value\n");
            continue;
        }

        /*
         * Convert string to long
         */
        errno = 0;
        char *extra;
        long value = strtol(input, &extra, 10);

        /*
         * Check for no extra values after the number
         */
        if (*extra != '\0') {
            printf("Value entered is not a number. Contains extra string \"%s\".\n", extra);
            continue;
        }

        /*
         * Check input value range
         */
        if (errno == ERANGE) {
            printf("Inputted string is out of range: %ld to %ld", LONG_MIN, LONG_MAX);
            continue;
        }

        // Search for value
        long index = linearSearch(value, numbers, fileNumberCount);

        if (index >= 0) {
            printf("%ld was found at position %ld\n\n", value, index);
        } else {
            printf("%ld was not found\n\n", value);
        }
    }

    puts("Bye\n");

    return EXIT_SUCCESS;
}
