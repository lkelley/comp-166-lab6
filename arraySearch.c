//
// Created by Landon on 2020-01-20.
//

#include "arraySearch.h"

/**
 * Searches an array for a value using a linear search
 *
 * @param value Value to search for
 * @param numbers Array of numbers to search
 * @param nNumbers Number of elements in the array
 * @return Returns the zero-based index of the element in the array, -1 if not found
 */
int linearSearch(const int value, const int *numbers, const int nNumbers) {
    /*
     * Iterate through the array to find the value
     */
    for (int i = 0; i < nNumbers; i++) {
        if (numbers[i] == value) return i;
    }

    // Return -1 if the value is not in the array
    return -1;
}
